#!/bin/bash
set -eou pipefail

# setup environment
source /etc/default/restic-backup

metrics_target="/tmp/metrics/restic-check-prune.prom"
metrics_tmp="$(mktemp)"

# check repo
check_start_ts="$(date +%s)"
restic check
check_end_ts="$(date +%s)"

cat > "$metrics_tmp" <<EOF
backup_check_duration $(( check_end_ts - check_start_ts ))
backup_check_end $check_end_ts
EOF

# prune old snapshots
prune_start_ts="$(date +%s)"
restic forget --keep-daily 7 --keep-weekly 5 --keep-monthly 12 --prune
prune_end_ts="$(date +%s)"

cat > "$metrics_tmp" <<EOF
backup_prune_duration $(( prune_end_ts - prune_start_ts ))
backup_prune_end $prune_end_ts
EOF

mv "$metrics_tmp" "$metrics_target"
