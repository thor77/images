#!/bin/sh
tmp="restic.bz2"
target="$image/restic"
wget -O "$tmp" "https://github.com/restic/restic/releases/download/v$version/restic_${version}_linux_amd64.bz2"
bzip2 -d "$tmp" -c > "$target"
chmod +x "$target"
