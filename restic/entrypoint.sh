#!/bin/bash
set -eou pipefail

source /etc/default/restic-backup

crontab_root="/var/spool/cron/crontabs/root"
echo "${BACKUP_CRON:-0 2 * * *} /usr/bin/restic-backup" > "$crontab_root"

if [[ "${BACKUP_PRUNE_ENABLED:-true}" == "true" ]]; then
    echo "0 4 * * 6 /usr/bin/restic-check-prune" >> "$crontab_root"
fi

crond -f -l 1
