#!/bin/bash
set -eou pipefail

# setup environment
source /etc/default/restic-backup

metrics_target="/tmp/metrics/restic.prom"
metrics_tmp="$(mktemp)"
start_ts="$(date +%s)"

# execute pre-script if exists, should write data to /tmp/backup
tmp_datadir=''
if [ -f /usr/local/bin/pre.sh ]; then
    tmp_datadir="/tmp/pre"
    mkdir -p "$tmp_datadir"
    /usr/local/bin/pre.sh "$tmp_datadir"
fi

# backup filesystem
restic backup /data $tmp_datadir

# cleanup tmp data
[ -n "$tmp_datadir" ] && rm -rf "$tmp_datadir"

end_ts=$(date +%s)

cat > "$metrics_tmp" <<EOF
backup_duration $(( end_ts - start_ts ))
backup_end $end_ts
EOF

mv "$metrics_tmp" "$metrics_target"
